package ca.consulti.fibodemo;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.math.BigInteger;

public class FiboService extends Service {
    public FiboService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private BigInteger calculateFibonacci(Integer val) {
        BigInteger a = BigInteger.ZERO;
        BigInteger b = BigInteger.ONE;
        int m = 0;
        for (int i = 31 - Integer.numberOfLeadingZeros(val); i >= 0; i--) {
            // Loop invariant: a = F(m), b = F(m+1)
            assert a.equals(slowFibonacci(m));
            assert b.equals(slowFibonacci(m+1));

            // Double it
            BigInteger d = multiply(a, b.shiftLeft(1).subtract(a));
            BigInteger e = multiply(a, a).add(multiply(b, b));
            a = d;
            b = e;
            m *= 2;
            assert a.equals(slowFibonacci(m));
            assert b.equals(slowFibonacci(m+1));

            // Advance by one conditionally
            if (((val >>> i) & 1) != 0) {
                BigInteger c = a.add(b);
                a = b;
                b = c;
                m++;
                assert a.equals(slowFibonacci(m));
                assert b.equals(slowFibonacci(m+1));
            }
        }
        return a;
    }

    /**
	 * Find one sum
	 *
	 */
    private static BigInteger slowFibonacci(int n) {
        BigInteger a = BigInteger.ZERO;
        BigInteger b = BigInteger.ONE;
        for (int i = 0; i < n; i++) {
            BigInteger c = a.add(b);
            a = b;
            b = c;
        }
        return a;
    }

    /**
     * More representative multiplication
     *
     */
    private static BigInteger multiply(BigInteger x, BigInteger y) {
        return x.multiply(y);
    }

    public static Thread performOnBackgroundThread(final Runnable runnable) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {

                }
            }
        };
        t.start();
        return t;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        final int n = intent.getIntExtra(getString(R.string.fibo_extra_value), 0);

        performOnBackgroundThread(new Runnable() {
            @Override
            public void run() {
                BigInteger res = calculateFibonacci(n);

                Intent retIntent = new Intent(getString(R.string.fibo_ready));
                retIntent.putExtra(getString(R.string.fibo_extra_value), res);
                sendBroadcast(retIntent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
