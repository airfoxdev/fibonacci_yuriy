package ca.consulti.fibodemo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;

public class MainActivity extends Activity {
    private EditText fiboInput;
    private TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View button = findViewById(R.id.fibo_button);
        fiboInput = (EditText) findViewById(R.id.fibo_input);
        output = (TextView) findViewById(R.id.fibo_value);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer fiboInteger;
                String fiboTarget = fiboInput.getText().toString();

                try {
                    fiboInteger = Integer.valueOf(fiboTarget);
                    if (fiboInteger < 1 || fiboInteger > 1000) {
                        throw new NumberFormatException();
                    }
                    calculate(fiboInteger);
                } catch (NumberFormatException nfe) {
                    Toast.makeText(MainActivity.this, "Unacceptable number. Please type in values in 1-1000 range", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(getString(R.string.fibo_ready));
        registerReceiver(receiver, filter);
    }


    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    void calculate(Integer fiboInteger) {
        Intent serviceIntent = new Intent(this, FiboService.class);
        serviceIntent.putExtra(getString(R.string.fibo_extra_value), fiboInteger);
        startService(serviceIntent);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equalsIgnoreCase(getString(R.string.fibo_ready))) {
                BigInteger valueFromService = (BigInteger)intent.getSerializableExtra(getString(R.string.fibo_extra_value));
                if (valueFromService.toString().length() < 0) {
                    output.setText(getString(R.string.calculation_failed));
                } else {
                    String calculationValue = ""+valueFromService;
                    output.setText(calculationValue);
                }
            }
        }
    };
}
